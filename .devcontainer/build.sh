#!/bin/sh

for dist in melodic noetic
do
    docker pull ros:$dist
    docker build --build-arg BASE_IMAGE=ros:$dist -f Dockerfile -t okdhryk/sciurus17-devcontainer-vscode:$dist-desktop .
done
