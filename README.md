# Sciurus17 dev container for Vscode
Yosuke Matsusaka (MID Academic Promotions, Inc.)さんを参考に、Visual Studio Code(vscode)のdevcontainer(Remote Container)を利用した、Sciurus17シミュレータ対応の開発環境を作りました。

https://github.com/devrt/ros-devcontainer-vscode

他いろいろなロボットシミュレータに関しては、[こちら](https://qiita.com/yosuke@github/items/328dbd778047499828f2)をご覧ください。


# はじめに
dockerさえインストールしてあれば、docker-compose upコマンド一発で[シミュレータ](https://gitlab.com/okadalaboratory/dev-sciurus17/simulator-sciurus17)と[開発環境](https://gitlab.com/okadalaboratory/dev-sciurus17/sciurus17-devcontainer-vscode)が立ち上がるようになっています。

# 使い方
## Dockerのインストール
初めのDockerをインストールします

### Windows11の場合

### Ubuntu20.04の場合


# レポジトリのチェックアウトとシミュレータの起動
## 下記の様に本レポジトリをチェックアウトします
```
$ git clone https://gitlab.com/okadalaboratory/dev-sciurus17/sciurus17-devcontainer-vscode.git
$ cd sciurus17-devcontainer-vscode
```

## シミュレータの起動
シミュレータは下記の様に docker-compseコマンドで起動できます。
```
docker-compose up
```
初回起動時はシミュレータと開発環境のDockerイメージがダウンロードされるので、起動までにしばらく時間がかかります。
しばらく待って環境が立ち上がったら、Webブラウザから以下の各URLを開いてみてください。

リモートサーバでdocker-compose upしている場合は、localhostの部分をサーバのIPアドレスで置き換えてください。

### シミュレータ画面
http://localhost:3000

### Web IDE（開発環境）
http://localhost:3001

### Juypter notebook
http://localhost:3002


# Dockerコンテナでの開発
Dockerコンテナ上で変更（ファイルの追加や修正）はコンテナが消えると反映されません。

この開発環境では、お使いのホストPCのカレントディレクトリをDockerコンテナ上では /workspace ディレクトリにマウントしています。

従って、　/workspace 配下のディレクトリにおける変更はホストPCのカレントディレクトリに反映されるため、再度、Dockerコンテナを起動し直しても変更が保たれます。

詳細は docker-compose.ymlをご覧ください。

