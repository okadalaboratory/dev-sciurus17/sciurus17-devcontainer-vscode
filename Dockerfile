# Copyright (c) 2022 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
FROM devrt/ros-devcontainer-vscode:noetic-desktop
LABEL maintainer="Hiroyuki Okada <hiroyuki.okada@okadanet.org>"
LABEL org.okadanet.vendor="Hiroyuki Okada" \
      org.okadanet.dept="TRCP" \
      org.okadanet.version="1.0.0" \
      org.okadanet.released="Feburary 7, 2023"

ENV DEBIAN_FRONTEND noninteractive
USER root

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    debconf-utils curl x11-apps apt-transport-https wget \
    && rm -rf /var/lib/apt/lists/*

RUN source /opt/ros/noetic/setup.bash && \
    mkdir -p /tmp/catkin_ws/src && cd /tmp/catkin_ws/src && \
    catkin_init_workspace && \
    git clone https://github.com/rt-net/sciurus17_ros.git &&\
    cd /tmp/catkin_ws && \
    sudo apt-get update && \
    rosdep install --from-paths src --ignore-src -r -y && \
    catkin_make -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/ros/noetic install 

#ユーザーを切り替える
USER developer 
WORKDIR /home/developer/
RUN rosdep update

RUN git clone --depth 1 https://github.com/osrf/gazebo_models.git /tmp/gazebo_models && \
    sudo cp -r /tmp/gazebo_models/cafe_table /usr/share/gazebo-11/models/ && \
    sudo cp -r /tmp/gazebo_models/first_2015_trash_can /usr/share/gazebo-11/models/ && \
    sudo cp -r /tmp/gazebo_models/mailbox /usr/share/gazebo-11/models/ && \
    sudo cp -r /tmp/gazebo_models/table_marble /usr/share/gazebo-11/models/ && \
    sudo rm -r /tmp/gazebo_models

RUN echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc && \
    echo "source /catkin_ws/devel/setup.bash" >> ~/.bashrc 

RUN rosdep update
RUN echo 'export ROS_IP=`hostname -i`' >> ~/.bashrc